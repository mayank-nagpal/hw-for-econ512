function [q] = demand(p,v);
q(1)=exp(v(1)-p(1))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
q(2)=exp(v(2)-p(2))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
q(3)=exp(v(3)-p(3))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
q(4)=1/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
end

