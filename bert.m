function [fval] = bert(p);
q(1)=exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
q(2)=exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
q(3)=exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
fval=zeros(3,1);
for i=1:3
fval(i) = -p(i)*(1-q(i))+1;
end

