function L=LikGQ(data,nodes,param)
mu=param(1);
sigma=param(2);
gamma=0;

Y = data.Y;
X = data.X;
Z = data.Z;
N = data.N;
T = data.T;

[x,w]=qnwnorm(nodes,mu,sigma);
k     = length(w);
u    = zeros(N);
Lit     = NaN(N,20);
for i=1:N
    for j=1:k
        beta_i=x(j,1);
        eps=beta_i*X(:,i)+gamma*Z(:,i)+u(i)*ones(T,1);
        F=(1+exp(-eps)).^-1;
        s(j,1)=prod((F.^Y(:,i)).*((ones(T,1)-F).^(ones(T,1)-Y(:,i))));
    end;
    h(i,1)=w'*s;
end;
L=-sum(log(h));
return;