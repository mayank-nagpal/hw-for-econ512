clear all

% Setting Options
%Nelden-Mead
optnm =optimset('Display','iter','MaxIter',2e5,'MaxFunEvals',5e5,'TolX',1e10); 

%Loading Data
load('hw4data.mat');
Y = data.Y;
X = data.X;
Z = data.Z;
N = data.N;
T = data.T;

%Setting initial values
param=[0.15 1 0;0 0 1]  
param1 =[0.15 1]

% Gaussian QUadrature
% Calculating Likelihood using 5 Nodes
LikGQ(data,param1,5)
% Calculating Likelihood using 10 Nodes
LikGQ(data,param1,10)
% Monte Carlo Integration
% Calculating Likelihood using 5 Nodes
LikMC(data,param1,20)

%Calculating optimal parameter values for the above three
[est,fval,exitflag,output] =fminunc(@(param)LikGQ(data,param1,5),init,optnm);
[est,fval,exitflag,output] =fminunc(@(param)LikGQ(data,param1,10),init,optnm);
[est,fval,exitflag,output] =fminunc(@(param)LikMC(data,param1,20),init,optnm);

%Maximizing the likelihood function using monte carlo methods bu allowing
%the u parameters to vary.
param=[0.15 1 0;0 0 1]  
[est,fval,exitflag,output] =fminunc(@(param)LikMC(data,param,20),init,optnm);




