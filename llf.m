function nLLF = llf(data,param,nn)
 
Y = data.Y';
X = data.X';
Z = data.Z';
N = data.N;
T = data.T;
 

mu=param(:,1);
gamma =0;
Sigma=[param(:,2) param(:,3)];

Sigma = Sigma*Sigma';
xr = qnwnorm(nn,mu,Sigma);
mvnrnd(mu,sigma,100)
k     = length(w);
 
beta_i = x(:,1);
u_i    = x(:,2);
eps   = NaN(N,k,T);
F      = NaN(N,k,T);
y      = NaN(N,k,T);
 
for t = 1:T
    
    eps(:,:,t)    = X(:,t)*beta_i'+gamma*Z(:,t)*ones(1,k)+ones(N,1)*u_i';
    F(:,:,t)      = (1+exp(-eps(:,:,t))).^(-1);
    y(:,:,t)      = Y(:,t)*ones(1,k);
    
end
           
L_it   = (F.^y).*((1-F).^(1-y));
L_i    = prod(L_it,3);
L_i      = L_i.*(ones(N,1)*phi')*w;
logL=log(L);
nLLF=sum(logL);
nLLF  = -nLLF;
