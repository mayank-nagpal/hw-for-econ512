function nLLF = LikGQ(data,param,nodes)
% defining parameters 
Y = data.Y;
X = data.X;
Z = data.Z;
N = data.N;
T = data.T;

% generating multivariate normally distributed random numbers 
mu=param(:,1);
gamma =0;
Sigma=param;
Sigma(:,1)=[]; 
Sigma = Sigma*Sigma';
nn    = nodes*ones(1,2);
[x,w] = qnwnorm(nn,mu,Sigma);
k     = length(w);
beta_i = x(:,1);
u_i    = x(:,2);
Lit     = NaN(N,k);
% calculating the Likelihood
for i=1:N
for j=1:k

    eps    = X(:,i)*beta_i(j)+gamma*Z(:,i)+ones(T,1)*u_i(j);
    F      = (1+exp(-eps)).^(-1);
    L_it   = (F.^Y(:,i)).*((1-F).^(1-Y(:,i)));
    Lit(i,j)   = prod(L_it,1);
    end;
end;
Lit1=Lit*w;
nLLF=-sum(log(Lit1));  











